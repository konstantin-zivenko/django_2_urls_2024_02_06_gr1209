from django.http import HttpRequest, HttpResponse
import datetime


def index(request: HttpRequest, number=22, second_number: int = 100) -> HttpResponse:
    now = datetime.datetime.now()
    print(f"Request param: {request.GET}")
    print(number)
    return HttpResponse(
        "<html>"
        "<h1>Hello World!</h1>"
        "<hr>"
        f"<p>The current time is {now}</p>"
        "<hr>"
        f"number = {number}"
        f"second number = {second_number}"
        "</html>"
    )
