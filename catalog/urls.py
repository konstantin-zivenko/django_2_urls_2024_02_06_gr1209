from django.urls import path

from .views import index


app_name = "catalog"

urlpatterns = [
    path("<int:number>/<int:second_number>/", index, name="index"),
]
